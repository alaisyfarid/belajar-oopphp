<?php 

	require_once("animal.php");
	require_once("frog.php");
	require_once("ape.php");
	$hewan = new Animal("shaun");
	echo "Nama Hewan = ".$hewan->name ."<br>";
	echo "Kaki Hewan = ".$hewan->legs ."<br>";
	echo "Darah Hewan = ".$hewan->cold_blooded ."<br> <br>";

	
	$hewan1 = new Frog("Katak");
	echo "Nama Hewan = ". $hewan1->name ."<br>";
	echo "Kaki Hewan = ". $hewan1->legs ."<br>";
	echo "Darah Hewan = ". $hewan1->cold_blooded ."<br>";
	$hewan1->jump() . "<br>";
	echo "<br> <br>";
	$hewan2 = new Ape("Sungokong");
	echo "Nama Hewan = ". $hewan2->name ."<br>";
	echo "Kaki Hewan = ". $hewan2->legs ."<br>";
	echo "Darah Hewan = ". $hewan2->cold_blooded ."<br>";
	$hewan2->yell() . "<br>";

 ?>